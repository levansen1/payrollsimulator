
package payroll;

public class Employee {
   private String name;
   private double hourlyWage;
   private int hoursWorked;
   
   Employee(){
       
   }
   
   Employee(String name, double hourlyWage, int hoursWorked,double bonus){
       this.name = name;
       this.hourlyWage = hourlyWage;
       this.hoursWorked = hoursWorked;
   }
   
   public double calculatePay(){
      return hoursWorked * hourlyWage;
   }
   
   public boolean isManager(){
       return false;
   }
    public boolean isEmployee(){
       return true;
   }
}





