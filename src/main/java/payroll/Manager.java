/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package payroll;


public class Manager extends Employee {
    private double bonus;
    
    
  Manager(){
      
  }
  
  Manager(String name, double hourlyWage, int hoursWorked, double bonus){
      super(name,hourlyWage,hoursWorked,bonus);
      this.bonus = bonus;
  }

    public double getBonus() {
        return bonus;
    }
    
    @Override
    public double calculatePay(){
     return super.calculatePay() + bonus;
}
      public boolean isManager(){
       return true;
   }
      public boolean isEmployee(){
          return false;
      }
}