/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package payroll;

public class PayrollSimulation {
    
    public static void main(String[] args){
        EmployeeFactory factory = EmployeeFactory.getInstance();
        
        Employee emp1 = factory.getEmployee("Mark", 12, 1300, 0);
         Employee emp2 = factory.getEmployee("Lucy", 30, 1920, 20);
        
        System.out.println(" Is this an Employee?  " + emp1.isEmployee() + " Is this a Manager? " +emp1.isManager()+" has a wage of " + emp1.calculatePay());
                
               
 System.out.println(" Is this an Employee?  " + emp2.isEmployee() + ", Is this a Manager? " +emp2.isManager()+", has a wage of " + emp2.calculatePay());
               
                
        
    }
    
    
    
}
