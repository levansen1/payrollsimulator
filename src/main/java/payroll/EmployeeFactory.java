/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package payroll;


public class EmployeeFactory {
  
    private static EmployeeFactory obj;
    
      private EmployeeFactory(){
        
    }
    
    public static EmployeeFactory getInstance(){
        if(obj == null){
            obj = new EmployeeFactory();
        } 
        return obj;
       
    }
    
    public Employee getEmployee(String name, double hourlyWage, int hoursWorked,double bonus){
     double total = hourlyWage*hoursWorked;
     
     if(total<=25000){
         return new Employee(name,hourlyWage,hoursWorked,bonus);
     }
     else if(total>25000){
         return new Manager(name,hourlyWage,hoursWorked,bonus);
     }
     return null;
}
    
}

